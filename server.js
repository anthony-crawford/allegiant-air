var restify = require("restify");
require("./node_app/utilities");
require("./node_app/db");
var getRequests =require("./node_app/getRequests");
var postRequests =require("./node_app/postRequests");
var deleteRequests =require("./node_app/deleteRequests")
var server = restify.createServer();
server.use(restify.acceptParser(server.acceptable));
server.use(restify.authorizationParser());
server.use(restify.dateParser());
server.use(restify.queryParser());
server.use(restify.jsonp());
server.use(restify.gzipResponse());
server.use(restify.bodyParser());


server.post(/^\/allegiant-api\/(.*)\/(.*)/,postRequests);
server.get(/^\/allegiant-api\/(.*)\/(.*)/,getRequests);
server.del(/^\/allegiant-api\/(.*)\/(.*)/,deleteRequests)

server.get(/.*/,
    restify.serveStatic({
        directory:'./public/',
        "default":"index.html"
    })
);

var port = process.env.PORT || 8080;
server.listen(port,function(){
	console.log("Server Started. Press Ctrl+c to quit server")
})