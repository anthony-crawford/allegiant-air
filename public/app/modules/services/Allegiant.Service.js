var Allegiant;
(function(Allegiant){
    (function(Service){
        
        var Home = (function(){
                            function Home($http,$q,$location,REST_API,RESTService){
                                this.$q = $q;
				                this.$http = $http;
				                this.$location = $location;
                                this.REST_API=REST_API
				                this.RESTService = RESTService;
                            }
							
							Home.prototype.getAllUsers = function(){
                                var that = this;
								return this.RESTService.get(this.REST_API.userGetAll).then(function(data){
                                    
                                                                                        var deferred = that.$q.defer();								
                                                                                        deferred.resolve(data);
                                                                                        return deferred.promise
                                                                      });								
							}
                            
                            Home.prototype.updateUser= function(data){
                                var that = this;
                                return this.RESTService.post(this.REST_API.userUpdate,data).then(function(data){
                                                                                      
                                                                                        var deferred = that.$q.defer();								
                                                                                        deferred.resolve(data);
                                                                                        return deferred.promise
                                                                      });
                            }
                            
                            Home.prototype.addUser= function(data){
                                var that = this;
                                return this.RESTService.post(this.REST_API.userAdd,data).then(function(data){
                                                                                       
                                                                                        var deferred = that.$q.defer();								
                                                                                        deferred.resolve(data);
                                                                                        return deferred.promise
                                                                      });
                            }
                            
                            Home.prototype.deleteUser = function(data){
                                var that = this;
                                return this.RESTService.delete(this.REST_API.userDelete,{userId:data}).then(function(data){
                                                                                       
                                                                                        var deferred = that.$q.defer();								
                                                                                        deferred.resolve(data);
                                                                                        return deferred.promise
                                                                      });
                            
                            }
                            
                            return Home;
                            
                    })();
					
		Service.Home = Home;
        
        
        var Upload = (function(){
                            function Upload($http,$q,$location,REST_API,RESTService){                                
                                this.$q = $q;
				                this.$http = $http;
				                this.$location = $location;
                                this.REST_API=REST_API
				                this.RESTService = RESTService;
                            }
                    
                        return Upload
            
                        })();
        
        Service.Upload = Upload;
    
    })(Allegiant.Service || (Allegiant.Service={}))
    var Service = Allegiant.Service;
	})(Allegiant||(Allegiant={}))
	
	allegiant.service("HomeService",['$http','$q','$location','REST_API','RESTService',Allegiant.Service.Home])
    allegiant.service("UploadService",['$http','$q','$location','REST_API','RESTService',Allegiant.Service.Upload])