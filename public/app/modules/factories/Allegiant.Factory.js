var Allegiant;
(function(Allegiant){
    (function(Factory){
		
			
       function RESTService($http,$q){
                                
								    var showLoader = function() {
                                                                    $("#loader").css("display", "block");
                                                                }
                                    var hideLoader = function() {
                                                                    $("#loader").css("display", "none");
                                                                }
									return {
										 		get : function(url){
																 var httpProperties ={    method: "GET",
												                                           url: url,
												                                           cache:false
												                                       }
											                    var that = this;
											                    var deferred = $q.defer();
                                                                showLoader();
																$http(httpProperties).success(function (response, status) {
											                                                    deferred.resolve(response);
                                                                                                hideLoader();
											
											                                            }).error(function (response, status) {
																						   deferred.reject("An error occured while fetching items");
                                                                                            hideLoader();
											                                            });
														       return deferred.promise;										 
									 			},
                                                
                                                post : function(url,data){
																 var httpProperties ={    method: "POST",
												                                           url: url,
												                                           cache:false
												                                       }
                                                                 if(data){
                                                                    httpProperties.data = data;
                                                                 }
											                    var that = this;
											                    var deferred = $q.defer();
                                                                showLoader();
																$http(httpProperties).success(function (response, status) {
											                                                    deferred.resolve(response);
                                                                                                hideLoader();
											
											                                            }).error(function (response, status) {
																						   deferred.reject("An error occured while fetching items");
                                                                                            hideLoader();
											                                            });
														       return deferred.promise;										 
									 			},
                                        
                                                delete: function(url,data){
                                                                var httpProperties ={    method: "DELETE",
												                                           url: url,
												                                           cache:false
																						   ,headers:data
												                                       }
                                                                
											                    var that = this;
											                    var deferred = $q.defer();
                                                                showLoader();
																$http(httpProperties).success(function (response, status) {
											                                                    deferred.resolve(response);
                                                                                                hideLoader();
											
											                                            }).error(function (response, status) {
																						   deferred.reject("An error occured while fetching items");
                                                                                            hideLoader();
											                                            });
														       return deferred.promise;	
                                                
                                                
                                                }
                                        
									
											}// End of Returned Object
                            }//End of RESTService Factory
							
							
                            
                    
					
		Factory.RESTService = RESTService;
    
    })(Allegiant.Factory || (Allegiant.Factory={}))
    var Factory = Allegiant.Factory;
	})(Allegiant||(Allegiant={}))
	
allegiant.service("RESTService",['$http','$q',Allegiant.Factory.RESTService])