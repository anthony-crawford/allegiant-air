var Allegiant;
(function(Allegiant){
    (function(Controller){      
         // Navigation Controller        
        var Navigation = (function(){
                                function Navigation($scope){
                                    this.$scope = $scope;
                                    $scope.Navigation = this;
                                }
                            return Navigation;
                    })();

        Controller.Navigation = Navigation;
        
         var Home = (function(){
                            function Home($scope,$timeout,HomeService){
                                this.$scope = $scope;
								this.HomeService = HomeService;
								this.users ={};
                                this.getAllUsers();
                                this.tableId="user-table";
                                this.$timeout =$timeout;
                                $scope.Home = this;
                                
								
                            }
                            Home.prototype.resetModels = function(){
                                this.add_email="";
                                this.add_first_name="";
                                this.add_last_name="";
                                this.add_ip="";
                                this.add_latitude="";
                                this.add_longitude="";
                            }
                            
                            Home.prototype.addUser = function(){
                            
                                        var that = this;
                                        var newUser ={
                                                        email:     this.add_email,
                                                        first_name: this.add_first_name,
                                                        last_name: this.add_last_name,
                                                        ip: this.add_ip,
                                                        latitude: this.add_latitude,
                                                        longitude: this.add_longitude
                                                    }

                                            this.HomeService.addUser(newUser).then(function(response){
                                                alert(response.data.message);
                                                $("#loader").css("display", "block");
                                                that.appendNewUser(response.data.insertid);
                                                that.$timeout(function(){
                                                    $("#loader").css("display", "none");
                                                },0)
                                                that.resetModels();
                                            });
                                        
                            }
							
							Home.prototype.getAllUsers = function(){
								var that = this;
								this.HomeService.getAllUsers().then(function(response){
									that.users = response.data.users;
                                    
								});
							}
                            
                            Home.prototype.updateGrid = function(index){
                                               
                                $("#loader").css("display", "block");
                                this.users.splice(index,1)
                                        this.$timeout(function(){
                                            $("#loader").css("display", "none");
                                        },0)
                            }
                            
                            Home.prototype.appendNewUser = function(id){
                                this.users.push({
                                                    email:this.add_email,
                                                    first_name:this.add_first_name,
                                                    last_name:this.add_last_name,
                                                    ip:this.add_ip,
                                                    longitude:this.add_longitude,
                                                    latitude:this.add_latitude,
                                                    id:id
                                                    
                                })
                                
                            }
                            
                            Home.prototype.getRowData = function(index){
                                
                                var nlRowFields= document.getElementById(this.tableId).tBodies[1].rows[index].cells;                                                           
                                var arRowFields = Array.prototype.slice.call(nlRowFields);
                                var returnData ={};
                                
                                arRowFields.forEach(function(item,index,array){
                                    
                                    if(item.children[0].id){
                                        returnData[item.children[0].id.replace(/(_[0-9]+)/gi, "")] = item.children[0].value;
                                    }else if(item.children[0].name){
                                        returnData[item.children[0].name.replace(/(_[0-9]+)/gi, "")] = item.children[0].value;
                                    }else{
                                    }
                                })
                                var returnMessageAndData = {};
                                
                                if(Object.keys(returnData).length = nlRowFields.length ){
                                    returnMessageAndData.success =true; 
                                    returnMessageAndData.data =returnData;
                                }else{
                                    returnMessageAndData.success =false; 
                                    returnMessageAndData.data =returnData;
                                }
                                
                                nlRowFields=null;
                                arRowFields=null;
                                returnData=null;
                                
                                
                                return returnMessageAndData;
                                
                                
                            }
                            
                            Home.prototype.updateUser = function(index){                                                                
                                var that = this;
                                var dataRow = this.getRowData(index);
                                
                               if(dataRow.success){
                                    this.HomeService.updateUser(dataRow.data).then(function(response){
                                        alert(response.data.message);
                                    });
                                }else{
                                    alert("Opps...not All keys assigned. Something went Wrong.");
                                }
                                
                            }
                            
                            Home.prototype.deleteUser = function(index){
                                var that = this;
                                var dataRow = this.getRowData(index);
                                
                               if(dataRow.success){
                                   var that=this;
                                    this.HomeService.deleteUser(dataRow.data.id).then(function(response){
                                        if(response.data.affectedRows==1){
                                            
                                             that.updateGrid(index);
                                        }
                                        
                                    });
                                }else{
                                    alert("Opps...not All keys assigned. Something went Wrong.");
                                }
                            }
							
                    return Home;
					})();
					
		Controller.Home = Home;
    
        
        
        var Upload = (function(){
                        function Upload($scope,$timeout,UploadService){
                                this.$scope = $scope;
								this.UploadService = UploadService;
                                this.$timeout =$timeout;
                                this.file=[];
                                this.percent="";
                                this.fileFilter=[{title : "CSVs", extensions : "csv"}];
                                this.uploadedCSV=[];
                            
                                var that = this;
                                this.$scope.$watch("Upload.percent",function(value){
                                })
                                $scope.Upload = this;
                            
                        }
                                    
                         Upload.prototype.uploadConfirmation = function(){
                             
                             var response = JSON.parse(arguments[1])
                             if(response.data.affectedRows >=1){
                                this.uploadedCSV =response.data.csv;
                             }else{
                                alert(response.data.message);
                             }
                             
                        }
                        Upload.prototype.uploadFailed = function(){
                            alert("File Uploaded Unsuccessfully")
                        }
        
                    return Upload
                    })();
        
        Controller.Upload =Upload;
        
    })(Allegiant.Controller || (Allegiant.Controller={}))
    var Controller = Allegiant.Controller;
})(Allegiant||(Allegiant={}))

allegiant.controller("NavigationController",['$scope',Allegiant.Controller.Navigation]) 
allegiant.controller("HomeController",['$scope','$timeout','HomeService',Allegiant.Controller.Home])
allegiant.controller("UploadController",['$scope','$timeout','UploadService',Allegiant.Controller.Upload])