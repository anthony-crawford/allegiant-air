var allegiant = angular.module("allegiant", ['ngRoute','ngResource','mgcrea.ngStrap','plupload.directive'])
                    .config(['$routeProvider', '$locationProvider', 'plUploadServiceProvider',function ($routeProvider, $locationProvider, plUploadServiceProvider ) {
                        
                                $routeProvider
                                    .when("/",{
                                        title:"Home",
                                        templateUrl:"app/templates/home.html",
                                        controller:'HomeController'
                                    
                                    })             
                                    .when("/upload",{
                                        title:"Upload",
                                        templateUrl:"app/templates/upload.html",
                                        controller:'UploadController'
                                        
                                    })
                                    .otherwise({ template: '404' });

                                $locationProvider.html5Mode(false);   
                        
                        
    plUploadServiceProvider.setConfig('flashPath', 'bower_components/plupload-angular-directive/plupload.flash.swf');
    plUploadServiceProvider.setConfig('silverLightPath', 'bower_components/plupload-angular-directive/plupload.silverlight.xap');
    plUploadServiceProvider.setConfig('uploadPath', '/allegiant-api/users/uploads');
                        
                        
								}]);		
allegiant.constant("REST_API", { 
                                     "userGet": "/allegiant-api/users/getuser",
									 "userGetAll": "/allegiant-api/users/getusers",
                                     "userUpdate": "/allegiant-api/users/updateuser",
                                     "userDelete":"/allegiant-api/users/deleteuser",
                                     "userAdd":"/allegiant-api/users/adduser"
                                }
							);