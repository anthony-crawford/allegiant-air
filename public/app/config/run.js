allegiant.run(function($rootScope, $location) {
	  $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        if(current.$$route && current.$$route.title){
            $rootScope.title = current.$$route.title;
        }            
     });
});