var mysql =require("mysql");

//We are Pooling Connections
db_conn = mysql.createPool({
  connectionLimit : 4,
  host     : 'localhost',
  user     : 'root',
  password : '',
  database:"cdb_8e935c09e5",
  port:3306,
  supportBigNumbers :true
    //,timezone:'Z' // Default is 'local' if property is removed
});

process.on('SIGINT', function() {
    db_conn.end();
    console.log("\nbye bye");
    process.exit();
});
