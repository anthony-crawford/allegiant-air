var queries = {
    getUser : "SELECT * From users Where  id= ?",
    getUsers : "SELECT * From users",
    updateUser : "Update users set email = ?, first_name=?, last_name=?,ip=?,latitude=?,longitude=?,updated_at=? where id = ?",
    deleteUser : "Delete From users where id = ?",
    addUser : "INSERT INTO users(email,first_name,last_name,ip,latitude,longitude,created_at) VALUES(?,?,?,?,?,?,?)",
    addMultiUser:"INSERT INTO users(email,first_name,last_name,ip,latitude,longitude,created_at) VALUES"
};
module.exports = queries;