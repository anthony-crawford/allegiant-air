var UserMgmt = function () {
   this.sql_strings = require("./query_strings.js");
    this.fs = require("fs");
    this.q = require('q');
    this.RuleEngine = require('node-rules');
    this.rules = [{
        "condition": function(R) {
            R.when(this && !(/(\.(com)|(net)$)/.test(this.params.email.toString() ) ));
        },
        "consequence": function(R) {
            this.result = false, this.message = "User's Email was inputed inncorrectly";
            R.stop();
        }
    }];


}


UserMgmt.prototype.getusers = function (req, res, next){

        db_conn.query(this.sql_strings.getUsers,[],function(err,rows,fields){
            if(err){
                ServerError(res,next);
                throw  err
            }
            
            if(rows.length >= 0){
                SuccessResponse(res,next,{users:rows});
            }
        })

}

UserMgmt.prototype.updateuser = function (req, res, next){
    var now=new Date()
    var stTimestamp = now.getFullYear()+"-"+(now.getMonth()+1)+"-"+now.getDate()+" "+now.getHours()+":"+now.getMinutes()+":"+now.getSeconds();
     db_conn.query(this.sql_strings.updateUser,
                   [req.params.email,req.params.first_name,req.params.last_name,
                    req.params.ip,req.params.latitude,req.params.longitude,stTimestamp,req.params.id],
                   function(err,rows,fields){
                            
                            if(err){
                                ServerError(res,next);
                                throw  err
                            }else{
                                if(rows.affectedRows==1){
                                    SuccessResponse(res,next,{message:"Users Updated!"});
                                 }else{
                                    SuccessResponse(res,next,{message:"User Was NOT Updated!"});
                                 }
                                
                            }
                        })
}

UserMgmt.prototype.adduser = function (req, res, next){
    
 var validate = new this.RuleEngine(this.rules),
    now = new Date(),
    stTimestamp = now.getFullYear()+"-"+(now.getMonth()+1)+"-"+now.getDate()+" "+now.getHours()+":"+now.getMinutes()+":"+now.getSeconds();
    
    validate.execute(req,function(result){  
                        if(result.result) 
                                  db_conn.query(this.sql_strings.addUser,
                                    [req.params.email,req.params.first_name,req.params.last_name,
                                        req.params.ip,req.params.latitude,req.params.longitude,stTimestamp],
                                    function(err,rows,fields){
                                                
                                                if(err){
                                                    ServerError(res,next);
                                                    throw  err
                                                }else{
                                                    if(rows.affectedRows==1){
                                                        SuccessResponse(res,next,{insertid:rows.insertId,message:"User Added!"});
                                                    }else{
                                                        SuccessResponse(res,next,{message:"User Was NOT Added!"});
                                                    }
                                                    
                                                }
                                            })
                        else 
                           SuccessResponse(res,next,{message:result.message});                        
                    });
                    


}

UserMgmt.prototype.deleteuser = function (req, res, next){
     db_conn.query(this.sql_strings.deleteUser,[req.headers.userid],function(err,rows,fields){
                            
                            if(err){
                                ServerError(res,next);
                                throw  err
                            }else{
                                
                                if(rows.affectedRows==1){
                                    SuccessResponse(res,next,{affectedRows:rows.affectedRows,message:"Users Deleted!"});
                                 }else{
                                    SuccessResponse(res,next,{affectedRows:rows.affectedRows,message:"User Was NOT Deleted!"});
                                 }
                                
                            }
                        })
}

UserMgmt.prototype.uploads =function(req, res, next){
        
        var now = new Date();
        var stTimestamp = now.getFullYear()+"-"+(now.getMonth()+1)+"-"+now.getDate()+" "+now.getHours()+":"+now.getMinutes()+":"+now.getSeconds();
        var that= this;
        var columns = ["createDatetime","ipAddress","geoLat","geoLong","firstName","lastName","emailAddress"];
    
        //Lets Load in the Uploaded CSV file
            require("csv-to-array")({
               file: req.files.file.path,
               columns: columns
            }, function (err, array) {
                //console.log(err || array);
                if(err){
                    throw err
                }else{
                    
                    //Get Rid of the Columns Headers
                   array.shift();
                    
                    //Build The Query String
                   var sqlMultiValues ="";
                   array.forEach(function(item,index,array){
                       
                            sqlMultiValues += "('"+(item.emailAddress || '')+"','"+(item.firstName ||'')+"','"+(item.lastName || '')+"','"+(item.ipAddress || '')+"',"+(item.geoLat || 0)+","+(item.geoLong || 0)+",'"+(item.createDatetime || stTimestamp)+"')";
                               if((array.length-1)!=index){
                                    sqlMultiValues +=",";
                               }
                          
                   });                          
                    
                   
                   var multiUsersInsert = that.sql_strings.addMultiUser +sqlMultiValues; 
                    
                    //Query and Return Results
                   db_conn.query(multiUsersInsert,[],function(err,rows,fields){
                            if(err){
                                ServerError(res,next);
                                throw  err
                            }else{
                                   if(rows.affectedRows>=1){
                                    SuccessResponse(res,next,{csv:array,affectedRows:rows.affectedRows,message:"File Uploaded and Users Added!"}); 
                                   }else{
                                    SuccessResponse(res,next,{csv:array,affectedRows:rows.affectedRows,message:"File Uploaded but Users were NOT Added!"}); 
                                   }
                                                                                             
                            }
                   });
                   
                }
                
            });
        /* var deferred = this.q.defer();
            this.fs.readFile(req.files, 'utf8', function (err,data) {
              if (err) {
                  deferred.reject(err)
              }else{	
                  deferred.resolve(data);
                }

            });
        return deferred.promise;*/
}

module.exports = UserMgmt;